<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['api']], function(){

    Route::post('/auth/signup', 'AuthController@signup');
    Route::post('/auth/signin', 'AuthController@signin');

    Route::group(['middleware' => ['jwt.auth']], function(){
        Route::post('profile', 'ProfileController@show');
        Route::post('pinjaman', 'PinjamanController@store');
        Route::group(['middleware' => ['admin']], function(){
            Route::post('pinjaman/{id}/update', 'PinjamanController@update');
            Route::post('/buku/store', 'BukuController@store');
            Route::post('/buku/{id}/update/', 'BukuController@update');
            Route::post('/buku/{id}/delete/', 'BukuController@destroy');
        });


    });
});
