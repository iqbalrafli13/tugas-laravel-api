<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePinjamansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pinjamans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('mahasiswa_id');
            $table->unsignedBigInteger('buku_id');
            $table->date('start_date');
            $table->date('end_date');
            $table->date('pengembalian')->nullable();
            $table->boolean('ontime')->default(0);
            $table->timestamps();

            $table->foreign('mahasiswa_id')
                  ->references('id')->on('mahasiswas')
                  ->onDelete('cascade');
            $table->foreign('buku_id')
                  ->references('id')->on('bukus')
                  ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pinjamans');
    }
}
