<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    protected $fillable = [
        "name", "nim", "fakultas", "jurusan", "nohp", "nowa","user_id"
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function pinjamans()
    {
        return $this->hasMany('App\Pinjamans');
    }


}
