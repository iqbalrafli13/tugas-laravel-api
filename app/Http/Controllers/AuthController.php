<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use JWTAuth;

use App\User;

class AuthController extends Controller
{
    public function signup(Request $request)
    {
        $this->validate($request,[
            "username"=>"required|unique:users",
            "email"=>"required|unique:users",
            "password"=>"required",
            "name"=>"required",
            "nim"=>"required",
            "fakultas"=>"required",
            "jurusan"=>"required",
            "nohp"=>"required",
            "nowa"=>"required",
        ]);

        $user = User::create([
            "username"=>$request->json("username"),
            "email"=>$request->json("email"),
            "password"=>bcrypt($request->json("password"))
        ]);

        Mahasiswa::create([
            "name"      =>$request->json("name"),
            "nim"       =>$request->json("nim"),
            "fakultas"  =>$request->json("fakultas"),
            "jurusan"   =>$request->json("jurusan"),
            "nohp"      =>$request->json("nohp"),
            "nowa"      =>$request->json("nowa"),
            "user_id"   =>$user->id
        ]);

        return [
            "username"=>$request->json("username"),
            "email"=>$request->json("email"),
            "name"      =>$request->json("name"),
            "nim"       =>$request->json("nim"),
            "fakultas"  =>$request->json("fakultas"),
            "jurusan"   =>$request->json("jurusan"),
            "nohp"      =>$request->json("nohp"),
            "nowa"      =>$request->json("nowa"),
        ];
    }
    public function signin(Request $request)
    {
        $this->validate($request,[
            "username"=>"required",
            "password"=>"required"
        ]);
        // grab credentials from the request
        $credentials = $request->only('username', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = $this->guard()->attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json([
            'user_id' => $request->user()->id,
            'token'   => $token
        ]);
    }

    public function logout()
    {
        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function guard()
    {
        return Auth::guard();
    }
}
