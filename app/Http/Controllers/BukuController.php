<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buku;

class BukuController extends Controller
{

    public function store(Request $request)
    {

        $this->validate($request,[
            "kodebuku"=>"required|unique:bukus",
            "judul"=>"required",
            "pengarang"=>"required",
            "tahun"=>"required",
        ]);

        return $buku = Buku::create([
            "kodebuku"=>$request->json('kodebuku'),
            "judul"=>$request->json('judul'),
            "pengarang"=>$request->json('pengarang'),
            "tahun"=>$request->json('tahun'),
        ]);

    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            "judul"=>"required",
            "pengarang"=>"required",
            "tahun"=>"required",
        ]);
        $buku = Buku::find($id);
        $buku->update([
            "judul"=>$request->json('judul'),
            "pengarang"=>$request->json('pengarang'),
            "tahun"=>$request->json('tahun'),
        ]);
        return $buku;
    }

    public function destroy($id)
    {
        $buku = Buku::find($id);
        $buku->delete();
        return "buku dihapus";
    }
}
