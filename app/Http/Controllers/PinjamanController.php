<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pinjaman;

class PinjamanController extends Controller
{
    public function store(Request $request)
    {

        $this->validate($request,[
            "buku_id"=>"required",
            "start_date"=>"required",
            "end_date"=>"required",

        ]);

        $pinjam = Pinjaman::create([
            'buku_id' => $request->json('buku_id'),
            'start_date'  => $request->json('start_date'),
            'end_date'  => $request->json('end_date'),
            'mahasiswa_id'  => $request->user()->mahasiswa->id
        ]);
        return $pinjam;
    }
    public function update(Request $request, $id)
    {
        $pinjam = Pinjaman::find($id);
        $this->validate($request,[
            'pengembalian'  => "required"
        ]);
        $ontime = 0;
        if ( strtotime($request->json('pengembalian')) < strtotime($pinjam->end_date)) {
            $ontime = 1;
        }
        $pinjam->update([
            'pengembalian'  => $request->json('pengembalian'),
            'ontime'  => $ontime

        ]);
        return $pinjam;


    }

}
