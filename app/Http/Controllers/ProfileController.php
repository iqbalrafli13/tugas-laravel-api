<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{


    public function show(Request $request)
    {
        return response()->json([
            'Nama'   => $request->user()->mahasiswa->name,
            'NIM'   => $request->user()->mahasiswa->nim,
            'Fakultas'   => $request->user()->mahasiswa->fakultas,
            'Jurusan'   => $request->user()->mahasiswa->jurusan,
            'No HP'   => $request->user()->mahasiswa->nohp,
            'No WA'   => $request->user()->mahasiswa->nowa,
        ]);

    }
}
