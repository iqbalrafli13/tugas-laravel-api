<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $fillable = [
        "kodebuku","judul","pengarang","tahun",
    ];

    public function pinjamans()
    {
        return $this->hasMany('App\Pinjamans');
    }
}
