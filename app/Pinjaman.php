<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pinjaman extends Model
{
    protected $table  = "pinjamans";
    protected $fillable = ["mahasiswa_id","buku_id","start_date","end_date","pengembalian" ,"ontime"];


    public function mahasiswa()
    {
        return $this->belongsToMany('App\Mahasiswa');
    }
    public function buku()
    {
        return $this->belongsTo('App\Buku');
    }
}
